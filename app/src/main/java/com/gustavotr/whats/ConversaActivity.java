package com.gustavotr.whats;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;
import com.gustavotr.adapter.MensagemAdapter;
import com.gustavotr.config.ConfiguracaoFirebase;
import com.gustavotr.helper.Base64Custom;
import com.gustavotr.helper.Preferencias;
import com.gustavotr.model.Conversa;
import com.gustavotr.model.Mensagem;

import java.util.ArrayList;

public class ConversaActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private EditText edit_mensagem;
    private ImageButton btn_enviar;
    private DatabaseReference firebase;
    private ListView listView;
    private ArrayList<Mensagem> mensagens;
    private ArrayAdapter<Mensagem> adapter;
    private ValueEventListener valueEventListenerMensagem;


    private String nomeUsuarioDestinatario;
    private String nomeUsuarioRemetente;
    private String idUsuarioRemetente;
    private String idUsuarioDestinatario;


    @Override
    protected void onStop() {
        super.onStop();
        firebase.removeEventListener(valueEventListenerMensagem);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_conversa);

        toolbar = (Toolbar) findViewById(R.id.tb_conversa);
        btn_enviar = (ImageButton) findViewById(R.id.btn_enviar);
        edit_mensagem = (EditText) findViewById(R.id.edit_mensagem);
        listView = (ListView) findViewById(R.id.lv_conversas);

        Preferencias preferencias = new Preferencias(ConversaActivity.this);
        idUsuarioRemetente = preferencias.getIdentificador();
        nomeUsuarioRemetente = preferencias.getNome();


        Bundle extra = getIntent().getExtras();

        if(extra != null ){
            nomeUsuarioDestinatario = extra.getString("nome");
            String emailDestinatario = extra.getString("email");
            idUsuarioDestinatario = Base64Custom.codificarBase64(emailDestinatario);
        }

        toolbar.setTitle(nomeUsuarioDestinatario);
        toolbar.setNavigationIcon(R.drawable.ic_action_arrow_left);
        setSupportActionBar(toolbar);

        mensagens = new ArrayList<>();

        adapter = new MensagemAdapter(ConversaActivity.this, mensagens);

        listView.setAdapter(adapter);

        firebase = ConfiguracaoFirebase.getFirebase()
                .child("mensagens")
                .child(idUsuarioRemetente)
                .child(idUsuarioDestinatario);

        valueEventListenerMensagem = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                mensagens.clear();

                for (DataSnapshot dados : dataSnapshot.getChildren() ){
                    Mensagem mensagem = dados.getValue( Mensagem.class );
                    mensagens.add( mensagem );
                }

                adapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        };


        firebase.addValueEventListener(valueEventListenerMensagem);

        btn_enviar.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                String textoMensagem = edit_mensagem.getText().toString();
                if(textoMensagem.isEmpty()){
                    Toast.makeText(ConversaActivity.this, "Digite uma mensagem para enviar!", Toast.LENGTH_SHORT).show();
                }else{
                    Mensagem mensagem = new Mensagem();
                    mensagem.setIdUsuario( idUsuarioRemetente );
                    mensagem.setMensagem( textoMensagem );

                    Boolean retornoMensagemRemetente = salvarMensagem(idUsuarioRemetente, idUsuarioDestinatario, mensagem);

                    if( !retornoMensagemRemetente ){
                        Toast.makeText(ConversaActivity.this, "Problema ao salvar mensagem, tente novamente!", Toast.LENGTH_SHORT).show();
                    }else{
                        Boolean retornoMensagemDestinatario = salvarMensagem(idUsuarioDestinatario, idUsuarioRemetente, mensagem);
                        if( !retornoMensagemDestinatario ){
                            Toast.makeText(ConversaActivity.this, "Problema ao enviar mensagem para o destinatario, tente novamente!", Toast.LENGTH_SHORT).show();
                        }
                    }

                    Conversa conversa = new Conversa();
                    conversa.setIdUsuario(idUsuarioDestinatario);
                    conversa.setNome(nomeUsuarioDestinatario);
                    conversa.setMensagem(textoMensagem);
                    Boolean retorno = salvarConversa(idUsuarioRemetente, idUsuarioDestinatario, conversa);
                    if( !retorno ){
                        Toast.makeText(ConversaActivity.this, "Problema ao salvar a conversa, tente novamente!", Toast.LENGTH_SHORT).show();
                    }else{
                        conversa = new Conversa();
                        conversa.setIdUsuario(idUsuarioRemetente);
                        conversa.setNome(nomeUsuarioRemetente);
                        conversa.setMensagem(textoMensagem);
                        retorno = salvarConversa(idUsuarioDestinatario, idUsuarioRemetente, conversa);

                        if( !retorno ) {
                            Toast.makeText(ConversaActivity.this, "Problema ao salvar a conversa para o destinatário, tente novamente!", Toast.LENGTH_SHORT).show();
                        }
                    }



                    edit_mensagem.setText("");
                }
            }
        });



    }

    private boolean salvarConversa(String idRemetente, String idDestinatario, Conversa conversa){
        try{
            firebase = ConfiguracaoFirebase.getFirebase().child("conversas");
            firebase.child( idRemetente )
                    .child( idDestinatario )
                    .setValue(conversa);
            return true;
        }catch(Exception e){
            e.printStackTrace();
            return false;
        }
    }

    private boolean salvarMensagem(String idRemetente, String idDestinarario, Mensagem mensagem ) {
        try{
            firebase = ConfiguracaoFirebase.getFirebase().child("mensagens");
            firebase.child(idRemetente)
                    .child(idDestinarario)
                    .push()
                    .setValue( mensagem );
            return true;
        }catch( Exception e){
            e.printStackTrace();
            return false;
        }
    }
}
